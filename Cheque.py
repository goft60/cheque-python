import math

Number_to_letter = { #Tableau stockant les mots composant les nombres en lettres
    'unite' : {1 : 'un',2 :'deux',3 : 'trois',4 : 'quatre',5 : 'cinq',6 : 'six',7 : 'sept',8 : 'huit',9 : 'neuf'},
    'dizaine' : {1 : 'dix',2 : 'vingt',3 : 'trente',4 : 'quarante',5 : 'cinquante',6 : 'soixante'},
    'exception' : {11 : 'onze',12 : 'douze',13 : 'treize',14 : 'quatorze',15 : 'quinze',16 : 'seize'},
    'tenPow' : {100 : 'cent',1000 : 'mille', 1000000 : 'million'},
    'monnaie' : {'euro','centime'},
}

def Saisie(): # Fonction de saisie du nombre
    nb = -1
    while(VerifNb(nb) == False or nb == -1):
        try :
            nb = float(input("Saississez votre valeur : \n"))
        except ValueError: # Si ce n'est pas un réel qui est saisi
            print("Veuillez saisir une valeur numérique")
    return nb

def VerifNb(nb): # Fonction de vérification des bornes
    boolean = False
    Borne_min = 0.01
    Borne_max = 10**9-Borne_min
    if (nb >= Borne_min and nb <= Borne_max):
        boolean = True
    return boolean

def splitPartsNumber(nb): # Repartition du nombre saisi dans un tableau séparant les entiers des décimales
    Part = []
    Part.append(int(nb))
    Part.append(round((nb-Part[0])*100,0)) # Arrondi à 0.01, au supérieur si nécessaire
    return Part

def splitToBloc(nb): # Découpe la partie entiere en 3 Bloc de centaines
    Bloc = {}
    for indice in range(3):
        decoup = 10**(6-indice*3)
        if nb >= decoup : #Inutile de découper si c'est égal à 0
            Bloc[decoup] = (int(nb/decoup))
            nb = nb - Bloc[decoup]*decoup
        else :
            Bloc[decoup] = 0 
    return Bloc
        
def splitToDigit(nb): # Découpe chaque blocs de 3 chiffres en 3 chiffres séparé
    Part = {}
    if nb > 99:
        Part[100]=int(nb/100)
        nb = int(nb-Part[100]*100)
    else :
        Part[100] = 0
    if (nb < 100 and nb >16):
        Part[10]=int(nb/10)
        nb = int(nb-Part[10]*10)
        Part[1]=int(nb)
    elif nb>0 :
        Part[10] = 0
        Part[1]=int(nb)
    else :
        Part[10] = 0
        Part[1] = 0
    return Part

def constructCent(digit, sentence, mille): # Création de la chaine centaine
    Unite = Number_to_letter['unite']
    TenPow = Number_to_letter['tenPow']
    if digit[100] > 1 : # Si il y a au moins deux centaines alors on écrit combien il y en a
        sentence = sentence + Unite[digit[100]] + " "
    if digit[100] >= 1 : # Si il y a au moins une centaine alors on écrit cent
        sentence = sentence + TenPow[100]
    if digit[10] == 0 and digit[1] == 0 and digit[100] > 1 and mille == False :
        sentence = sentence + "s"
    else:
        if digit[10] > 0: # Si il y a une dizaine qui suit on va allez l'écrire
            sentence = constructDiz(digit,sentence, mille)
        elif digit[1] > 0 : # Sinon si il y a une unité qui suit on va allez l'écrire
            sentence = sentence + " " 
            sentence = constructUnit(digit,sentence)
    return sentence
        
def constructDiz(digit,sentence, mille): # Création de la chaine dizaine
    Unite = Number_to_letter['unite']
    exception = False #Pour un nombre entre 70-79 et 90-99 on a pas besoin d'appeler construcUnit
    Dizaine = Number_to_letter['dizaine']
    if digit[10] < 7 and digit[1]<10: # Si la dizaine est compris entre 6 et 1
        sentence = sentence + " " + Dizaine[digit[10]]
    if digit[10] == 7 : # construction début 70
        exception = True
        sentence = sentence +" "+ Dizaine[6]
    if digit[10] >= 8: # construction 80 et début 90
        sentence = sentence + " " + Unite[4]+"-"+Dizaine[2]
        if digit[10] == 9:
            exception = True
    if digit[10] >= 2 and digit[10] <= 7 and digit[1] == 1: #Ajout du 'et' pour 21, 31, 41, 51, 61, 71
        sentence = sentence + "-et"
    if digit[10] >=7: # Suite de la construction de 70 et 90
        sentence = constructExcep(digit,sentence,mille)
    if digit[1] > 0 and exception == False: # Si la dizaine est suivie par une unité
        sentence = sentence+"-"
        sentence = constructUnit(digit,sentence)
    return sentence

def constructUnit(digit,sentence): # Création de la chaine unité
    Exeption = Number_to_letter['exception'] # faute d'orthographe volontaire car Exception est déja pris
    Unite = Number_to_letter['unite']
    Dizaine = Number_to_letter['dizaine']
    if digit[1]<10: # construction de 1 à 9
        sentence = sentence + Unite[digit[1]]
    if digit[1]>10 and digit[1]<17: # construction des exceptions de 11 à 16
        sentence = sentence + Exeption[digit[1]]
    if digit[1] == 10 :
        sentence = sentence + Dizaine[1]
    return sentence

def constructExcep(digit,sentence, mille): # Création des exceptions (ex : 70, 90, ...)
    Exeption = Number_to_letter['exception'] # faute d'orthographe volontaire
    Unite = Number_to_letter['unite']
    Dizaine = Number_to_letter['dizaine']
    if (digit[10] == 8 and digit[1] == 0 and mille == False) :
        sentence = sentence + "s"
    if (digit[10] == 7 or digit[10] == 9) and (digit[1] <7 and digit[1]>0): # Construction de 71 à 76 et de 91 à 96
        sentence = sentence +"-"+Exeption[10+digit[1]]
    elif digit[10] == 7 or digit[10] == 9 : # Construction de 77 à 79 et de 97 à 99 et de 70 / 90
        sentence = sentence + "-"+Dizaine[1]
        if digit[1] >0:
            sentence = sentence+"-"+Unite[digit[1]]
    return sentence
        
def assemble(digits_array, part_int, part_dec): #Fonction construisant assemblant les mots de chaque partie du nombre (euros et centimes) retournant le résultat final
    sentence = ""
    
    if part_int > 0: #On s'occupe de la partie entière
        count_loop = 1
        for number in [6, 3]: #Pour chaque puissance de 10 : (millions, mille) à savoir les puissances où on a besoin de rajouter quelque chose après le résultat de constructCent
            not_mille = not(number == 3 and digits_array[count_loop][1] == 1 and digits_array[count_loop][10] == 0 and digits_array[count_loop][100] == 0)
            if digits_array[count_loop][1] + digits_array[count_loop][10] + digits_array[count_loop][100]  > 0: #On vérifie s'il y a besoin d'afficher cette partie en lettres
                if not_mille == True: #S'il ne s'agit de mille quelque chose euros (mille trois, mille dix euros)
                    sentence = constructCent(digits_array[count_loop], sentence, number == 3)
                    sentence = sentence + " "
                sentence = sentence +  Number_to_letter['tenPow'][10**number]
                if number == 6 and part_int > 1000000: #Plus d'un million
                    sentence = sentence + "s"
                if not_mille == True:
                    sentence = sentence + " "
            count_loop = count_loop + 1
        sentence = constructCent(digits_array[3], sentence, False) #On relance une dernière fois pour les centaines
        if part_int % 1000000 == 0: #S'il s'agit de millions ronds alors on dit "deux millions d'euros", "trois millions d'euros" etc...
            sentence = sentence + "d'"
        elif part_int % 1000000 != 0 and part_int % 1000 != 0:
            sentence = sentence + " "
        sentence = sentence + "euro" 
        if part_int > 1:
            sentence = sentence + 's'
        if part_dec > 0: #On affiche un et si jamais il y a aussi des centimes
            sentence = sentence + " et"
    if part_dec > 0: #On s'occupe de la partie décimalesentence = sentence + " et"
        digits_dec = splitToDigit(part_dec) #On découpe la partie décimale en digits
        sentence = constructCent(digits_dec, sentence, False) #On construit le mot
        sentence = sentence + " centime" #Et on ajoute la monnaie
        if part_dec > 1:
            sentence = sentence + "s"
    return sentence
# Main
Part = splitPartsNumber(Saisie())
Ent = splitToBloc(Part[0])
number_len = len(Ent)
Digits = {}
for indice in range(number_len):
    Digits[number_len-indice] = splitToDigit(Ent[10**(indice*3)])
sentence = assemble(Digits, Part[0], Part[1])
print(sentence)